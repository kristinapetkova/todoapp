Welcome to my test project 

# To build the project
make test build release

# To create a docker image tag for docker hub
make tag id=<your tag id>

# To publish the image
make publish 

#and to clean up
make clean


-----------
# development commands
# to run the server
cd src; python manage.py runserver --settings=todobackend.settings.test 
cd src; python manage.py runserver --settings=todobackend.settings.localtest

# to run the integration tests
cd src; python manage.py test

# to run the acceptance tests
1. start the server with localtest settings
2. go to todobackend-specs
3. start the following command: mocha

# to start the user frontend
1. start the server with localtest settings
2. build the project todobackend-client with: npm install
3. start npm app.js
4. Navigate to http://localhost:3000

# build docker with the host network
docker build -f docker/development/Dockerfile -t todobackend-dev --network=host .
docker run --rm --network host todobackend-dev

# build docker with the standart bridge network
cd ~/projects/todobackend; docker build -f docker/dev/Dockerfile -t todobackend-dev .
docker run -e DJANGO_SETTINGS_MODULE=todobackend.settings.test -e http_proxy="http://172.17.0.1:3128" -e https_proxy="http://172.17.0.1:3128" todobackend-dev

# einmalig um den cache zu mappen
docker run -v /tmp/cache --entrypoint true --name cache --network host todobackend-dev 
docker run --rm --volumes-from cache --network host todobackend-dev

# funktioniert noch nicht weil ich die settings innerhalb vom docker file fest gelegt habe
docker run --rm -e DJANGO_SETTINGS_MODULE=todobackend.settings.base --network host todobackend-dev

# docker compose does not run yet but will be fixed when the networking is up
cd docker/development
docker-compose build
docker-compose up agent
docker-compose up test

# clean up docker containers and images
docker rm $(docker ps -qa --no-trunc --filter "status=exited")
docker rmi $(docker images | grep "none" | awk '/ / { print $3 }')