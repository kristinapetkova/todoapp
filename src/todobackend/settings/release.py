from base import *
import os

if os.environ.get('DEBUG'):
 DEBUG = True
else:
 DEBUG = False

ALLOWED_HOSTS = [os.environ.get('ALLOWED_HOSTS'), '*']

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': { 
         # extract environment variables
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('MYSQL_DATABASE', 'todobackend'),
        'USER': os.environ.get('MYSQL_USER', 'todouser'),
        'PASSWORD': os.environ.get('MYSQL_PASSWORD', 'password'),
        'HOST': os.environ.get('MYSQL_HOST', '172.17.0.1'),
        'PORT': os.environ.get('MYSQL_PORT', '3306'),
    }
}

STATIC_ROOT = [os.environ.get('STATIC_ROOT'), '/var/www/todobackend/static']
STATIC_MEDIA = [os.environ.get('STATIC_ROOT'), '/var/www/todobackend/media']