PROJECT_NAME ?= todobackend
ORG_NAME ?= eyesfree
REPO_NAME ?= todobackend

DEV_FILE := docker/development/docker-compose.yml
REL_FILE := docker/release/docker-compose.yml

REL_PROJECT := ${PROJECT_NAME}${BUILD_NUMBER}
DEV_PROJECT := ${REL_PROJECT}_dev

DOCKER_REGISTRY ?= docker.io

DOCKER_REGISTRY_AUTH ?= 

BUILD_TAG_EXPRESSION ?= date -u +%Y%m%d%H%M%S
BUILD_EXPRESSION := $(shell $(BUILD_TAG_EXPRESSION))
BUILD_TAG ?= $(BUILD_EXPRESSION)

# app is the service name of our main application
APP_CONTAINER_ID := $$(docker-compose -p todobackend -f docker/release/docker-compose.yml ps -q app)

IMAGE_ID := $$(docker inspect -f '{{ .Image }}' $(APP_CONTAINER_ID))

REPO_EXPR := $$(docker inspect -f '{{ range .RepoTags }}{{.}} {{end}}' $(IMAGE_ID) | grep -oh "eyesfree/todobackend[^[:space:]|\$$]*" | xargs)

YELLOW := "\e[1;33m"
NC := "\e[0m"

INFO := @bash -c '\
	printf $(YELLOW); \
	echo "=> $$1"; \
	printf $(NC)' VALUE

INSPECT := $$(docker-compose -p $$1 -f $$2 ps -q $$3 | xargs -I ARGS docker inspect -f "{{ .State.ExitCode }}" ARGS)

CHECK := @bash -c '\
	if [[ $(INSPECT) -ne 0 ]];\
	then exit $(INSPECT); fi' VALUE


.PHONY: test build release clean tag buildtag

update:
	${INFO} "Updating the image with code changes"
	docker build -f docker/development/Dockerfile -t eyesfree/todobackend-dev .
	docker login
	docker push eyesfree/todobackend-dev:latest

test:
	${INFO} "Making test Pulling latest images"
	docker-compose -p ${DEV_PROJECT} -f ${DEV_FILE} pull --ignore-pull-failures
	docker-compose -p ${DEV_PROJECT} -f ${DEV_FILE} build --pull
	${INFO} "Making test start the test service"
	docker-compose -p ${DEV_PROJECT} -f ${DEV_FILE} up test
	docker cp $$(docker-compose -p ${DEV_PROJECT} -f ${DEV_FILE} ps -q test):/test-reports/. test-reports
	${CHECK} $(DEV_PROJECT) $(DEV_FILE) test

build: 
	${INFO} "Making build build and run the build service"
	docker-compose -p ${DEV_PROJECT} -f ${DEV_FILE} build builder
	docker-compose -p ${DEV_PROJECT} -f ${DEV_FILE} up builder
	${CHECK} $(DEV_PROJECT) $(DEV_FILE) builder
	docker cp $$(docker-compose -p ${DEV_PROJECT} -f ${DEV_FILE} ps -q builder):/wheelhouse/. target

release: 
	${INFO} "Making release Pulling latest images"
	docker-compose -p ${REL_PROJECT} -f ${REL_FILE} pull test
	docker-compose -p ${REL_PROJECT} -f ${REL_FILE} build app
	docker-compose -p ${REL_PROJECT} -f ${REL_FILE} build webroot
	docker-compose -p ${REL_PROJECT} -f ${REL_FILE} build --pull nginx
	# docker-compose -p ${REL_PROJECT} -f ${REL_FILE} run --rm app manage.py migrate --no-input
	# docker-compose -p ${REL_PROJECT} -f ${REL_FILE} up app
	docker-compose -p ${REL_PROJECT} -f ${REL_FILE} up test
	${INFO} "Making release Done!"

clean: 
	${INFO} "Making clean removing containers, volumes, images"
	docker-compose -f ${DEV_FILE} --force -v rm
	docker-compose -f ${REL_FILE} --force -v rm
	docker images -q -f dangling=true -f label=application | xargs -I ARGS docker rmi -f ARGS
	
tag: 
	echo "you passed the tag name $(id)"
	docker tag $(IMAGE_ID) $(DOCKER_REGISTRY)/$(ORG_NAME)/$(REPO_NAME):$(id)

buildtag: 
	echo "you passed the tag name $(id) and it will be suffixed accordingly"
	docker tag $(IMAGE_ID) $(DOCKER_REGISTRY)/$(ORG_NAME)/$(REPO_NAME):$(id).$(BUILD_TAG)

login:
	docker login -u $$DOCKER_USER -p $$DOCKER_PASSWORD $(DOCKER_REGISTRY_AUTH)

logout:
	docker logout

publish: 
	@ $(foreach tag, $(shell echo $(REPO_EXPR)), docker push $(tag);)



